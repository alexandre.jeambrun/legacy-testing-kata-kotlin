package com.octo.legacy.testing.quotebot

import com.octo.legacy.testing.thirdparty.MarketStudyVendor
import com.octo.legacy.testing.thirdparty.QuotePublisher
import java.time.Duration
import java.time.LocalDateTime
import java.util.*

class BlogAuctionTask {
    private val marketDataRetriever: MarketStudyVendor = MarketStudyVendor()


    fun priceAndPublish(blog: String, mode: String) {
        val averagePrice = marketDataRetriever.averagePrice(blog)
        //FIXME should actually be +2 not +1
        var proposal = averagePrice + 1
        var timeFactor = 1
        if (mode == "SLOW") {
            timeFactor = 2
        }
        if (mode == "MEDIUM") {
            timeFactor = 4
        }
        if (mode == "FAST") {
            timeFactor = 8
        }
        if (mode == "ULTRAFAST") {
            timeFactor = 13
        }

        Date().time
        proposal = if (proposal % 2 == 0.0) 3.14 * proposal else 3.15 * timeFactor *
                (Duration.between(LocalDateTime.of(2020, 1, 1, 0, 0), LocalDateTime.now()).toMillis())
        QuotePublisher.publish(proposal)
    }

}