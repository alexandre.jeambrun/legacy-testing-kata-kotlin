package com.octo.legacy.testing.quotebot

class TechBlogs {
    companion object {
        @JvmStatic
        fun listAllBlogs() : List<String> {
            Thread.sleep(5000) //Database call is very long
            return listOf(
                "HackerNews", "Reddit", "TechCrunch", "BuzzFeed",
                "TMZ", "TheHuffPost", "GigaOM"
            )
        }
    }
}