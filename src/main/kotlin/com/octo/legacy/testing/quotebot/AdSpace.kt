package com.octo.legacy.testing.quotebot

class AdSpace {
    companion object {
        private val cache = HashMap<String, List<String>>()
        @JvmStatic
        fun getAdSpaces() : List<String> {
            if (cache.containsKey("blogs list")) {
                return cache["blogs list"].orEmpty()
            }

            val listOfBlogs = TechBlogs.listAllBlogs()
            cache.put("blogs list", listOfBlogs)
            return cache["blogs list"].orEmpty()
        }
    }
}