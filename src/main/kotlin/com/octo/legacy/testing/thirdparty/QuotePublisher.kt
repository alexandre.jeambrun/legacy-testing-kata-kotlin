package com.octo.legacy.testing.thirdparty

import javax.swing.JOptionPane

object QuotePublisher {
    fun publish(proposal: Double) {
        JOptionPane.showMessageDialog(
            null,
            "You've pushed a dummy auction to a real ads platform, the business is upset!", "Big fail!",
            JOptionPane.WARNING_MESSAGE
        )
    }

}